﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CubeController : MonoBehaviour
{
    [SerializeField] private GameObject _mainCube;
    [SerializeField]private float _speed = 0;
    private float _spentTime =0;

    private List<Transform> _neighbors;

    public static Action<Transform> MoveCubeEvent;

    private void OnEnable()
    {
        MoveCubeEvent += MovingCubeToTarget;
        Pathfinder.SetNeighbors += GetNeighbors;
    }

    private void OnDisable()
    {
        MoveCubeEvent -= MovingCubeToTarget;
        Pathfinder.SetNeighbors -= GetNeighbors;
    }

    private void MovingCubeToTarget(Transform targetPosition)
    {
        StopAllCoroutines();
        if (Pathfinder.s_enablePathfinder)
        {
            Pathfinder.StartPathfinging(_mainCube.transform, targetPosition);
            _neighbors.Add(targetPosition);
            StartCoroutine(NeighborMoveCubeCor(_neighbors.ToArray()));
        }
        else
        {
             StartCoroutine(MoveCubeCor(targetPosition));
        }
    }

    private IEnumerator MoveCubeCor(Transform targetPosition)
    {
        Vector3 startPosition = _mainCube.transform.position;
        Vector3 startAngle = _mainCube.transform.eulerAngles;
        _spentTime = 0;

        while(_mainCube.transform.position != targetPosition.position)
        {
            _mainCube.transform.position = BezierPath.GetBezierPoint(startPosition, startPosition + new Vector3(0, -3, 0), targetPosition.position + new Vector3(0, -3, 0), targetPosition.position, _spentTime * _speed);
            _mainCube.transform.rotation = Quaternion.Euler(BezierPath.GetTargetRotation(startAngle,targetPosition.eulerAngles, _spentTime * _speed));
            _spentTime += 1f * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator NeighborMoveCubeCor(Transform[] targetPosition)
    {
        for (int i = 0; i < targetPosition.Length; i++)
        {
            Vector3 startPosition = _mainCube.transform.position;
            Vector3 startAngle = _mainCube.transform.eulerAngles;
            _spentTime = 0;

            while (_mainCube.transform.position != targetPosition[i].position)
            {
                _mainCube.transform.position = BezierPath.GetBezierPoint(startPosition, startPosition + new Vector3(0, -3, 0), targetPosition[i].position + new Vector3(0, -3, 0), targetPosition[i].position, _spentTime * _speed);
                _mainCube.transform.rotation = Quaternion.Euler(BezierPath.GetTargetRotation(startAngle, targetPosition[i].eulerAngles, _spentTime * _speed));
                _spentTime += 1f * Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }
    }

    private void GetNeighbors(List<Transform> transforms)
    {
        _neighbors = transforms;
    }
}
