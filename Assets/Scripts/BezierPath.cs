﻿using UnityEngine;

public class BezierPath: MonoBehaviour
{
    public static Vector3 GetBezierPoint(Vector3 startPoint, Vector3 bezierPoint1, Vector3 bezierPoint2, Vector3 endPoint, float t)
    {
        t = Mathf.Clamp01(t);
        float reverseT = 1f - t;
        return reverseT * reverseT * reverseT * startPoint + 3f * reverseT * reverseT * t * bezierPoint1 + 3f * reverseT * t * t * bezierPoint2 + t * t * t * endPoint;
    }

    public static Vector3 GetTargetRotation(Vector3 startAngle, Vector3 endAngle,float t)
    {
        t = Mathf.Clamp01(t);
        float reverseT = 1f - t;
        return startAngle * reverseT + endAngle * t;
    }
}
