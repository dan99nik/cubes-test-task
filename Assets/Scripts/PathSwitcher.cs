﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PathSwitcher : MonoBehaviour
{
    [SerializeField]private Text _text;

    private void SwitchPathfinerActive()
    {
        if (Pathfinder.s_enablePathfinder)
        {
            Pathfinder.s_enablePathfinder = false;
        }
        else
        {
            Pathfinder.s_enablePathfinder = true;
        }
        _text.text = "Pathfinder: " + Pathfinder.s_enablePathfinder;
    }
}
