﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Pathfinder : MonoBehaviour
{
    public static bool s_enablePathfinder = false;

    [SerializeField] private GameObject _parentNeighbors;
    private Transform[] _allNeighbors;

    private List<Transform> _findedTransforms = new List<Transform>();

    public static Action<Transform, Transform> StartPathfinging;
    public static Action<List<Transform>> SetNeighbors;

    private void OnEnable()
    {
        StartPathfinging += Pathfinding;
    }

    private void OnDisable()
    {
        StartPathfinging -= Pathfinding;
    }

    private void Start()
    {
        _allNeighbors = new Transform[_parentNeighbors.transform.childCount];
        for (int i = 0; i < _allNeighbors.Length; i++)
        {
            _allNeighbors[i] = _parentNeighbors.transform.GetChild(i);
        }
    }

    private void Pathfinding(Transform startTransform, Transform endTransform)
    {
        _findedTransforms = new List<Transform>();
        FindNearestNeighbor(startTransform, endTransform);
    }

    private void FindNearestNeighbor(Transform startTransform, Transform endTransform)
    {
        List<Transform> localNeighbor = new List<Transform>();

        float minDist = float.MaxValue;

        for (int i = 0; i < _allNeighbors.Length; i++)
        {
            if (Vector3.Distance(startTransform.position, _allNeighbors[i].position) <= minDist && startTransform != _allNeighbors[i])
            {
                if (Vector3.Distance(startTransform.position, endTransform.position) >= Vector3.Distance(_allNeighbors[i].position, endTransform.position))
                {
                    minDist = Vector3.Distance(startTransform.position, _allNeighbors[i].position);
                }
            }
        }
        for (int i = 0; i < _allNeighbors.Length; i++)
        {
            if (Vector3.Distance(startTransform.position, _allNeighbors[i].position) == minDist && startTransform != _allNeighbors[i])
            {
                localNeighbor.Add(_allNeighbors[i]);
            }
        }

        float minDistToTarget = float.MaxValue;
        int neighborID = 0;
        for (int i = 0; i < localNeighbor.Count; i++)
        {
            if (Vector3.Distance(localNeighbor[i].position, endTransform.position) < minDistToTarget)
            {
                minDistToTarget = Vector3.Distance(localNeighbor[i].position, endTransform.position);
                neighborID = i;
            }
        }

        bool endTransformReached = true;
        for (int i = 0; i < _allNeighbors.Length; i++)
        {
            if (Vector3.Distance(_allNeighbors[i].position, endTransform.position) < minDistToTarget)
            {
                _findedTransforms.Add(localNeighbor[neighborID]);
                FindNearestNeighbor(localNeighbor[neighborID], endTransform);
                endTransformReached = false;
                break;
            }
        }

        if (endTransformReached)
        {
            if (localNeighbor[neighborID] != endTransform)
            {
                _findedTransforms.Add(localNeighbor[neighborID]);
            }
            SetNeighbors?.Invoke(_findedTransforms);
        }
    }
}
