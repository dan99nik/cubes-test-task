﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] private Transform _cubeBasePostion;
    private int _layerMask = 1 << 8;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity,_layerMask))
            {
                CubeController.MoveCubeEvent?.Invoke(hit.transform);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CubeController.MoveCubeEvent?.Invoke(_cubeBasePostion);
        }
    }
}
